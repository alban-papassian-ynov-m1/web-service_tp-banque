import { AccountDto } from "../src/models/dto/account.dto";

export interface JwtPayload {
    id: string;
    username: string;
    accounts: AccountDto[];
}
