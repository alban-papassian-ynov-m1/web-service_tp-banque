import { Controller, HttpCode, Param, Get, Post, Body, Delete, UseGuards } from "@nestjs/common";
import { ApiUseTags, ApiOperation, ApiResponse, ApiBearerAuth } from "@nestjs/swagger";
import { FindOneOptions } from "typeorm";
import { GenericResponse } from "../models/responses/_generic.response";
import { AuthGuard } from "@nestjs/passport";
import { TransfertsService } from "../services/transferts.service";
import { GetTransferResponse, GetTransfersResponse } from "../models/responses/transfer.responses";
import { Transfert } from "../entities/transfert.entity";
import { TransfertDto } from "../models/dto/transfert.dto";

@Controller('transferts')
@ApiUseTags('transferts')
export class TransfertsController {
    constructor(
        private readonly transfertsService: TransfertsService,
    ) { }

    @UseGuards(AuthGuard('jwt'))
    @Get(':transfertId')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get transfert', operationId: 'getTransfert' })
    @ApiResponse({ status: 200, description: 'Get transfert', type: GetTransferResponse })
    @HttpCode(200)
    async get(): Promise<GetTransferResponse> {
        const findOneOptions: FindOneOptions<Transfert> = {};
        return await this.transfertsService.findOne(findOneOptions);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all transferts', operationId: 'getAllTransferts' })
    @ApiResponse({ status: 200, description: 'Get all transferts', type: GetTransfersResponse })
    @HttpCode(200)
    async getAll(): Promise<GetTransfersResponse> {
        return await this.transfertsService.findAll();
    }

    @Post()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create or update transfer', operationId: 'createOrUpdateTransfert' })
    @ApiResponse({ status: 200, description: 'Create or update transfer', type: GetTransferResponse })
    @HttpCode(200)
    async createOrUpdate(@Body() transfert: TransfertDto): Promise<GetTransferResponse> {
        return await this.transfertsService.createOrUpdate(transfert);
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete(':transfertId')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete transfert', operationId: 'deleteTransfert' })
    @ApiResponse({ status: 200, description: 'Delete transfert', type: GenericResponse })
    @HttpCode(200)
    async delete(@Param('transfertId') transfertId: string): Promise<GenericResponse> {
        return await this.transfertsService.delete(transfertId);
    }
}
