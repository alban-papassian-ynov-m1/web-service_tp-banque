import { Controller, Post, Body, HttpCode } from "@nestjs/common";
import { ApiUseTags, ApiOperation, ApiResponse, ApiBearerAuth } from "@nestjs/swagger";
import { AuthService } from "../services/auth.service";
import { LoginViewModel } from "../models/login-viewmodel";
import { GenericResponse } from "../models/responses/_generic.response";
import { RegisterRequest } from "../models/requests/register.request";

@Controller('auth')
@ApiUseTags('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
    ) { }

    @Post('login')
    @ApiOperation({ title: 'Login user', operationId: 'login' })
    @ApiResponse({ status: 200, description: 'Login response', type: GenericResponse })
    @HttpCode(200)
    async login(@Body() loginViewModel: LoginViewModel): Promise<GenericResponse> {
        return await this.authService.login(loginViewModel);
    }

    @Post('register')
    @ApiOperation({ title: 'register', operationId: 'register' })
    @ApiResponse({ status: 200, description: 'Generic Response', type: GenericResponse })
    @HttpCode(200)
    @ApiBearerAuth()
    async register(@Body() request: RegisterRequest): Promise<GenericResponse> {
        return await this.authService.register(request);
    }
}
