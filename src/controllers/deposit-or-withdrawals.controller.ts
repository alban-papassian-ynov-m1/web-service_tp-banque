import { Controller, HttpCode, Param, Get, Post, Body, Delete, UseGuards } from "@nestjs/common";
import { ApiUseTags, ApiOperation, ApiResponse, ApiBearerAuth } from "@nestjs/swagger";
import { FindOneOptions } from "typeorm";
import { GenericResponse } from "../models/responses/_generic.response";
import { AuthGuard } from "@nestjs/passport";
import { DepositOrWithdrawalsService } from "../services/deposit-or-withdrawals.service";
import { GetDepositOrWithdrawalResponse, GetDepositOrWithdrawalsResponse } from "../models/responses/deposit-or-withdrawal.responses";
import { DepositOrWithdrawal } from "../entities/deposit-or-withdrawal.entity";
import { DepositOrWithdrawalDto } from "../models/dto/deposit-or-withdrawal.dto";

@Controller('deposit-or-withdrawal')
@ApiUseTags('deposit-or-withdrawal')
export class DepositOrWithdrawalsController {
    constructor(
        private readonly depositsService: DepositOrWithdrawalsService,
    ) { }

    @UseGuards(AuthGuard('jwt'))
    @Get(':depositId')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get deposit or withdrawal', operationId: 'getDepositOrWithdrawal' })
    @ApiResponse({ status: 200, description: 'Get deposit or withdrawal', type: GetDepositOrWithdrawalResponse })
    @HttpCode(200)
    async get(): Promise<GetDepositOrWithdrawalResponse> {
        const findOneOptions: FindOneOptions<DepositOrWithdrawal> = {};
        return await this.depositsService.findOne(findOneOptions);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all deposit or withdrawal', operationId: 'getAllDepositOrWithdrawal' })
    @ApiResponse({ status: 200, description: 'Get all deposit or withdrawal', type: GetDepositOrWithdrawalsResponse })
    @HttpCode(200)
    async getAll(): Promise<GetDepositOrWithdrawalsResponse> {
        return await this.depositsService.findAll();
    }

    @Post()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create or update deposit or withdrawal', operationId: 'createOrUpdateDepositOrWithdrawal' })
    @ApiResponse({ status: 200, description: 'Create or update deposit or withdrawal', type: GetDepositOrWithdrawalResponse })
    @HttpCode(200)
    async createOrUpdate(@Body() deposit: DepositOrWithdrawalDto): Promise<GetDepositOrWithdrawalResponse> {
        return await this.depositsService.createOrUpdate(deposit);
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete(':depositId')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete deposit or withdrawal', operationId: 'deleteDepositOrWithdrawal' })
    @ApiResponse({ status: 200, description: 'Delete deposit or withdrawal', type: GenericResponse })
    @HttpCode(200)
    async delete(@Param('depositId') depositId: string): Promise<GenericResponse> {
        return await this.depositsService.delete(depositId);
    }
}
