import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne } from 'typeorm';
import { Account } from './account.entity';
import { TransfertDto } from '../models/dto/transfert.dto';

@Entity({ name: 'transferts' })
export class Transfert {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    id: string;

    @Column('float', { name: 'amount', nullable: false })
    amount: number;

    @Column('datetime', { name: 'date', nullable: false })
    date: Date;

    @Column('varchar', { name: 'targetAccountNb', nullable: false })
    targetAccountNb: string;

    @Column('varchar', { name: 'accountId', nullable: false })
    accountId: string;

    @ManyToOne(() => Account, account => account.transferts)
    @JoinColumn({ name: 'accountId' })
    account: Account;

    public toDto(): TransfertDto {
        return {
            id: this.id,
            date: this.date,
            amount: this.amount,
            targetAccountNb: this.targetAccountNb,

            accountId: this.accountId,

            account: this.account ? this.account.toDto() : null,
        };
    }

    public fromDto(dto: TransfertDto) {
        this.id = dto.id;
        this.date = dto.date;
        this.amount = dto.amount;
        this.targetAccountNb = dto.targetAccountNb;

        this.accountId = dto.accountId;

        if (dto.account) {
            const account = new Account();
            account.fromDto(dto.account);
            this.account = account;
            this.accountId = account.id;
        }

        if (!this.id) {
            this.id = undefined;
        }
    }
}
