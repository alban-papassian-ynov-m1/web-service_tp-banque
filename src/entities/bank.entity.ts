import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Account } from './account.entity';
import { BankDto } from '../models/dto/bank.dto';

@Entity({ name: 'banks' })
export class Bank {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    id: string;

    @Column('varchar', { name: 'name', nullable: false })
    name: string;

    @Column('varchar', { name: 'number', nullable: false })
    number: string;

    @OneToMany(() => Account, account => account.bank, { cascade: true, nullable: true })
    accounts: Account[];

    public toDto(): BankDto {
        return {
            id: this.id,
            name: this.name,
            number: this.number,

            accounts: this.accounts ? this.accounts.map(x => x.toDto()) : [],
        };
    }

    public fromDto(dto: BankDto) {
        this.id = dto.id;
        this.name = dto.name;
        this.number = dto.number;

        if (dto.accounts) {
            this.accounts = [];
            dto.accounts.forEach(accountDto => {
                const account = new Account();
                account.fromDto(accountDto);
                this.accounts.push(account);
            });
        }

        if (!this.id) {
            this.id = undefined;
        }
    }
}
