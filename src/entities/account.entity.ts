import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { Bank } from './bank.entity';
import { User } from './user.entity';
import { AccountDto } from '../models/dto/account.dto';
import { Transfert } from './transfert.entity';
import { DepositOrWithdrawal } from './deposit-or-withdrawal.entity';

@Entity({ name: 'accounts' })
export class Account {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    id: string;

    @Column('float', { name: 'balance', nullable: false })
    balance: number;

    @Column('float', { name: 'limit', nullable: false })
    limit: number;

    @Column('varchar', { name: 'number', nullable: false })
    number: string;

    @Column('varchar', { name: 'pinCode', nullable: false })
    pinCode: string;

    @Column('varchar', { name: 'bankId', nullable: false })
    bankId: string;

    @ManyToOne(() => Bank, bank => bank.accounts)
    @JoinColumn({ name: 'bankId' })
    bank: Bank;

    @Column('varchar', { name: 'userId', nullable: false })
    userId: string;

    @ManyToOne(() => User, user => user.accounts)
    @JoinColumn({ name: 'userId' })
    user: User;

    @OneToMany(() => Transfert, transfert => transfert.account, { cascade: true, nullable: true })
    transferts: Transfert[];

    @OneToMany(() => DepositOrWithdrawal, depositOrWithdrawal => depositOrWithdrawal.account, { cascade: true, nullable: true })
    depositOrWithdrawals: DepositOrWithdrawal[];

    public toDto(): AccountDto {
        return {
            id: this.id,
            limit: this.limit,
            balance: this.balance,
            number: this.number,
            pinCode: this.pinCode,

            bankId: this.bankId,
            userId: this.userId,

            bank: this.bank ? this.bank.toDto() : null,
            user: this.user ? this.user.toDto() : null,
            transferts: this.transferts ? this.transferts.map(x => x.toDto()) : [],
            depositOrWithdrawals: this.depositOrWithdrawals ? this.depositOrWithdrawals.map(x => x.toDto()) : [],
        };
    }

    public fromDto(dto: AccountDto) {
        this.id = dto.id;
        this.limit = dto.limit;
        this.balance = dto.balance;
        this.number = dto.number;
        this.pinCode = dto.pinCode;

        this.bankId = dto.bankId;
        this.userId = dto.userId;

        if (dto.bank) {
            const bank = new Bank();
            bank.fromDto(dto.bank);
            this.bank = bank;
            this.bankId = bank.id;
        }

        if (dto.user) {
            const user = new User();
            user.fromDto(dto.user);
            this.user = user;
            this.userId = user.id;
        }

        if (dto.transferts) {
            this.transferts = [];
            dto.transferts.forEach(transfertDto => {
                const transfert = new Transfert();
                transfert.fromDto(transfertDto);
                this.transferts.push(transfert);
            });
        }

        if (dto.depositOrWithdrawals) {
            this.depositOrWithdrawals = [];
            dto.depositOrWithdrawals.forEach(depositOrWithdrawalDto => {
                const depositOrWithdrawal = new DepositOrWithdrawal();
                depositOrWithdrawal.fromDto(depositOrWithdrawalDto);
                this.depositOrWithdrawals.push(depositOrWithdrawal);
            });
        }

        if (!this.id) {
            this.id = undefined;
        }
    }
}
