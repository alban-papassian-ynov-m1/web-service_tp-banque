import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { UserDto } from '../models/dto/user.dto';
import { Account } from './account.entity';

@Entity({ name: 'users' })
export class User {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    id: string;

    @Column('varchar', { name: 'username', nullable: false })
    username: string;

    @Column('varchar', { name: 'password', nullable: false })
    password: string;

    @Column('varchar', { name: 'firstname', nullable: false })
    firstname: string;

    @Column('varchar', { name: 'lastname', nullable: false })
    lastname: string;

    @Column('datetime', { name: 'birthDate', nullable: false })
    birthDate: Date;

    @Column('varchar', { name: 'civility', nullable: false })
    civility: string;

    @OneToMany(() => Account, account => account.bank, { cascade: true, nullable: true })
    accounts: Account[];

    public toDto(getPassword?: boolean): UserDto {
        return {
            id: this.id,
            firstname: this.firstname,
            lastname: this.lastname,
            birthDate: this.birthDate,
            civility: this.civility,
            username: this.username,
            password: getPassword ? this.password : undefined,

            accounts: this.accounts ? this.accounts.map(x => x.toDto()) : [],
        };
    }

    public fromDto(dto: UserDto) {
        this.id = dto.id;
        this.firstname = dto.firstname;
        this.lastname = dto.lastname;
        this.birthDate = dto.birthDate;
        this.civility = dto.civility;
        this.username = dto.username;

        if (dto.accounts) {
            this.accounts = [];
            dto.accounts.forEach(accountDto => {
                const account = new Account();
                account.fromDto(accountDto);
                this.accounts.push(account);
            });
        }

        if (!this.id) {
            this.id = undefined;
        }
    }
}
