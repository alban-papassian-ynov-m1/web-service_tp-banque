import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UsersController } from './controllers/users.controller';
import { UsersService } from './services/users.service';
import { User } from './entities/user.entity';
import { Transfert } from './entities/transfert.entity';
import { DepositOrWithdrawal } from './entities/deposit-or-withdrawal.entity';
import { Bank } from './entities/bank.entity';
import { Account } from './entities/account.entity';
import { TransfertsController } from './controllers/transferts.controller';
import { DepositOrWithdrawalsController } from './controllers/deposit-or-withdrawals.controller';
import { TransfertsService } from './services/transferts.service';
import { DepositOrWithdrawalsService } from './services/deposit-or-withdrawals.service';
import { Constants } from '../environments/constants';
import { AuthController } from './controllers/auth.controller';
import { AuthService } from './services/auth.service';
import { AuthToolsService } from './services/auth-tools.service';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: Constants.jwtsecret,
      signOptions: { expiresIn: '3650d' },
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'tp_banque',
      logging: false,
      entities: [
        User,
        Transfert,
        DepositOrWithdrawal,
        Bank,
        Account,
      ],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([
      User,
      Transfert,
      DepositOrWithdrawal,
      Bank,
      Account,
    ]),
  ],
  controllers: [
    AppController,
    AuthController,
    UsersController,
    TransfertsController,
    DepositOrWithdrawalsController,
  ],
  providers: [
    AppService,
    AuthService,
    AuthToolsService,
    UsersService,
    TransfertsService,
    DepositOrWithdrawalsService,
  ],
})
export class AppModule { }
