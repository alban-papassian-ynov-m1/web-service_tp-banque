import { Injectable } from '@nestjs/common';
import { GenericResponse } from '../models/responses/_generic.response';
import { Repository, FindManyOptions, FindOneOptions } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { DepositOrWithdrawal } from '../entities/deposit-or-withdrawal.entity';
import { GetDepositOrWithdrawalsResponse, GetDepositOrWithdrawalResponse } from '../models/responses/deposit-or-withdrawal.responses';
import { DepositOrWithdrawalDto } from '../models/dto/deposit-or-withdrawal.dto';

@Injectable()
export class DepositOrWithdrawalsService {
    constructor(
        @InjectRepository(DepositOrWithdrawal)
        private readonly depositRepository: Repository<DepositOrWithdrawal>,
    ) {
        //
    }

    async findAll(conditions?: FindManyOptions<DepositOrWithdrawal>): Promise<GetDepositOrWithdrawalsResponse> {
        const response = new GetDepositOrWithdrawalsResponse();
        try {
            conditions.relations = [];
            const deposits = await this.depositRepository.find(conditions);

            if (deposits) {
                response.depositOrWithdrawals = deposits.map(x => x.toDto());
                response.totalDepositOrWithdrawalsCount = await this.depositRepository.count(conditions);
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async findOne(conditions: FindOneOptions<DepositOrWithdrawal>): Promise<GetDepositOrWithdrawalResponse> {
        const response = new GetDepositOrWithdrawalResponse();
        try {
            conditions.relations = [];
            const deposit = await this.depositRepository.findOne(conditions);
            if (deposit) {
                response.depositOrWithdrawal = deposit.toDto();
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async createOrUpdate(deposit: DepositOrWithdrawalDto): Promise<GetDepositOrWithdrawalResponse> {
        const response = new GetDepositOrWithdrawalResponse();
        try {
            let depositEntity = await this.depositRepository.findOne({ id: deposit.id }, { relations: [] });
            if (!depositEntity) {
                depositEntity = new DepositOrWithdrawal();
            }

            depositEntity.fromDto(deposit);

            depositEntity = await this.depositRepository.save(depositEntity);
            depositEntity = await this.depositRepository.findOne({ id: depositEntity.id });

            response.depositOrWithdrawal = depositEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async delete(depositId: string): Promise<GenericResponse> {
        const response = new GenericResponse();
        try {
            const deposit = await this.depositRepository.findOne({ id: depositId }, { relations: [] });
            if (!deposit)
                throw new Error('No deposit or withdrawal with id => ' + depositId);

            await this.depositRepository.delete(depositId);

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }
}
