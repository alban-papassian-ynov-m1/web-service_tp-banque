import { Inject, Injectable, Scope, ExecutionContext, Optional } from '@nestjs/common';
import { Request } from 'express';
import { REQUEST } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { UserDto } from '../models/dto/user.dto';
import { JwtPayload } from '../../environments/jwt-payload';

@Injectable({ scope: Scope.REQUEST })
export class AuthToolsService {

    constructor(
        @Optional() @Inject(REQUEST) private readonly request: Request,
        public readonly jwtService: JwtService,
    ) { }

    public static createUserToken(jwtService: JwtService, user: UserDto) {
        if (!user)
            return null;

        const payload: JwtPayload = { id: user.id, username: user.username, accounts: user.accounts };
        return jwtService.sign(payload, { expiresIn: '1day' });
    }

    public static getRequestFromContext(context: ExecutionContext): any {
        if (!context)
            return null;
        const httpContext = context.switchToHttp();
        if (!httpContext)
            return null;
        const request = httpContext.getRequest();
        if (!request)
            return null;
        return request;
    }

    public static getJwtPayloadFromRequest(jwtService: JwtService, request: any, ignoreExpiration: boolean): JwtPayload {
        if (!request || !request.headers || !request.headers.authorization)
            return null;
        return AuthToolsService.getJwtPayloadFromAuthHeader(jwtService, request.headers.authorization, ignoreExpiration);
    }

    public static getJwtPayloadFromAuthHeader(jwtService: JwtService, authorizationHeader: string, ignoreExpiration: boolean): JwtPayload {
        if (authorizationHeader && authorizationHeader.indexOf('Bearer') !== -1) {
            const tokenArray = authorizationHeader.split('Bearer ');
            if (tokenArray.length > 1) {
                const token = tokenArray[1];
                return AuthToolsService.decodeToken(jwtService, token, ignoreExpiration);
            }
        }
        return null;
    }
    public static decodeToken(jwtService: JwtService, encodedToken: string, ignoreExpiration: boolean): JwtPayload {
        let decoded: JwtPayload = null;
        try {
            decoded = jwtService.verify(encodedToken, { ignoreExpiration });
        } catch (err) {
            console.log(err);
        }
        return decoded;
    }

    public getCurrentPayload(ignoreExpiration: boolean): JwtPayload {
        if (this.request)
            return AuthToolsService.getJwtPayloadFromRequest(this.jwtService, this.request, ignoreExpiration);
        return null;
    }
}
