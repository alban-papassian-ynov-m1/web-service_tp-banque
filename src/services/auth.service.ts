import { Injectable } from '@nestjs/common';
import { UsersService } from './users.service';
import { JwtService } from '@nestjs/jwt';
import { AuthToolsService } from './auth-tools.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { GenericResponse } from '../models/responses/_generic.response';
import { LoginViewModel } from '../models/login-viewmodel';
import { AppError, AppErrorWithMessage } from '../models/app-error';
import { RegisterRequest } from '../models/requests/register.request';
import { UserDto } from '../models/dto/user.dto';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private readonly usersService: UsersService,
        public readonly jwtService: JwtService,
        public readonly authToolsService: AuthToolsService,
    ) { }

    async login(loginViewModel: LoginViewModel): Promise<GenericResponse> {
        const response: GenericResponse = new GenericResponse();
        try {
            if (!loginViewModel.password || !loginViewModel.username)
                throw AppError.getBadRequestError();

            const findUserResponse = await this.usersService.findOne({
                where: [{ username: loginViewModel.username }, { email: loginViewModel.username }],
            }, true);
            if (!findUserResponse.success)
                throw new AppError(findUserResponse.error);
            if (!findUserResponse.user)
                throw new AppErrorWithMessage('Utilisateur ou mot de passe incorrect !', 403);
            if (loginViewModel.password !== findUserResponse.user.password)
                throw new AppErrorWithMessage('Utilisateur ou mot de passe incorrect !', 403);
            response.token = AuthToolsService.createUserToken(this.jwtService, findUserResponse.user);
            response.success = true;
        } catch (err) {
            response.handleError(err);
        }
        return response;
    }

    async refreshToken(): Promise<GenericResponse> {
        const response: GenericResponse = new GenericResponse();
        try {
            const payload = this.authToolsService.getCurrentPayload(true);
            // console.log(': AuthService -> payload', payload);
            if (!payload)
                throw new AppErrorWithMessage('Utilisateur ou mot de passe incorrect !', 403);
            const findUserResponse = await this.usersService.findOne({ where: { id: payload.id } }, true);
            if (!findUserResponse.success)
                throw new AppError(findUserResponse.error);
            if (!findUserResponse.user)
                throw new AppErrorWithMessage('Utilisateur ou mot de passe incorrect !', 403);
            response.token = AuthToolsService.createUserToken(this.jwtService, findUserResponse.user);
            response.success = true;
        } catch (err) {
            response.handleError(err);
        }
        return response;
    }

    async register(request: RegisterRequest): Promise<GenericResponse> {
        let response: GenericResponse = new GenericResponse();
        try {
            if (!request.password)
                throw new AppErrorWithMessage('Impossible de créer un compte sans mot de passe.');

            const userResponseUsername = await this.usersService.findOne({
                where: {
                    username: request.username,
                },
            });
            if (!userResponseUsername.success)
                throw new AppError(userResponseUsername.error);
            if (userResponseUsername.user)
                throw new AppErrorWithMessage('Un compte existe déjà avec cette identifiant !');

            const user = new UserDto();
            user.password = request.password;
            user.username = request.username;
            user.birthDate = request.birthDate;
            user.civility = request.civility;
            user.firstname = request.firstname;
            user.lastname = request.lastname;

            response = await this.usersService.createOrUpdate(user, true);
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }
}
