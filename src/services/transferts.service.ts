import { Injectable } from '@nestjs/common';
import { GenericResponse } from '../models/responses/_generic.response';
import { Repository, FindManyOptions, FindOneOptions } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Transfert } from '../entities/transfert.entity';
import { GetTransfersResponse, GetTransferResponse } from '../models/responses/transfer.responses';
import { TransfertDto } from '../models/dto/transfert.dto';

@Injectable()
export class TransfertsService {
    constructor(
        @InjectRepository(Transfert)
        private readonly transfertRepository: Repository<Transfert>,
    ) {
        //
    }

    async findAll(conditions?: FindManyOptions<Transfert>): Promise<GetTransfersResponse> {
        const response = new GetTransfersResponse();
        try {
            conditions.relations = [];
            const transfers = await this.transfertRepository.find(conditions);

            if (transfers) {
                response.transfers = transfers.map(x => x.toDto());
                response.totalTransfersCount = await this.transfertRepository.count(conditions);
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async findOne(conditions: FindOneOptions<Transfert>): Promise<GetTransferResponse> {
        const response = new GetTransferResponse();
        try {
            conditions.relations = [];
            const transfert = await this.transfertRepository.findOne(conditions);
            if (transfert) {
                response.transfer = transfert.toDto();
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async createOrUpdate(transfert: TransfertDto): Promise<GetTransferResponse> {
        const response = new GetTransferResponse();
        try {
            let transferEntity = await this.transfertRepository.findOne({ id: transfert.id }, { relations: [] });
            if (!transferEntity) {
                transferEntity = new Transfert();
            }

            transferEntity.fromDto(transfert);

            transferEntity = await this.transfertRepository.save(transferEntity);
            transferEntity = await this.transfertRepository.findOne({ id: transferEntity.id });

            response.transfer = transferEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async delete(transfertId: string): Promise<GenericResponse> {
        const response = new GenericResponse();
        try {
            const transfer = await this.transfertRepository.findOne({ id: transfertId }, { relations: [] });
            if (!transfer)
                throw new Error('No transfer with id => ' + transfertId);

            await this.transfertRepository.delete(transfertId);

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }
}
