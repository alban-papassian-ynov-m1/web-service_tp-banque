import { Injectable } from '@nestjs/common';
import { GetUsersResponse, GetUserResponse } from '../models/responses/user.responses';
import { UserDto } from '../models/dto/user.dto';
import { JwtService } from '@nestjs/jwt';
import { AuthToolsService } from './auth-tools.service';
import { GenericResponse } from '../models/responses/_generic.response';
import { User } from '../entities/user.entity';
import { Repository, FindManyOptions, FindOneOptions } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        public readonly jwtService: JwtService,
    ) {
        //
    }

    async findAll(conditions?: FindManyOptions<User>): Promise<GetUsersResponse> {
        const response = new GetUsersResponse();
        try {
            conditions.relations = [];
            const users = await this.userRepository.find(conditions);

            if (users) {
                response.users = users.map(x => x.toDto());
                response.totalUsersCount = await this.userRepository.count(conditions);
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async findOne(conditions: FindOneOptions<User>, getPassword?: boolean): Promise<GetUserResponse> {
        const response = new GetUserResponse();
        try {
            conditions.relations = [];
            const user = await this.userRepository.findOne(conditions);
            if (user) {
                response.user = user.toDto(getPassword);
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async createOrUpdate(user: UserDto, mustGenerateToken: boolean): Promise<GetUserResponse> {
        const response = new GetUserResponse();
        try {
            let userEntity = await this.userRepository.findOne({ id: user.id }, { relations: [] });
            if (!userEntity) {
                userEntity = new User();
            }

            userEntity.fromDto(user);

            userEntity = await this.userRepository.save(userEntity);
            userEntity = await this.userRepository.findOne({ id: userEntity.id });

            response.user = userEntity.toDto();

            if (mustGenerateToken)
                response.token = AuthToolsService.createUserToken(this.jwtService, response.user);

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async delete(userId: string): Promise<GenericResponse> {
        const response = new GenericResponse();
        try {
            const user = await this.userRepository.findOne({ id: userId }, { relations: [] });
            if (!user)
                throw new Error('No user with id => ' + userId);

            await this.userRepository.delete(userId);

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }
}
