import { ApiModelProperty } from '@nestjs/swagger';

export class LoginViewModel {
    @ApiModelProperty()
    username: string;
    @ApiModelProperty()
    password: string;
}
