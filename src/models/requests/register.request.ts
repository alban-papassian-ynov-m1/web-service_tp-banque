import { ApiModelProperty } from "@nestjs/swagger";

export class RegisterRequest {
    @ApiModelProperty({
        description: "password of user",
        required: false,
        type: String,
    })
    password?: string;

    @ApiModelProperty({
        description: "username of user",
        required: false,
        type: String,
    })
    username?: string;

    @ApiModelProperty({
        description: "firstname of user",
        required: false,
        type: String,
    })
    firstname?: string;

    @ApiModelProperty({
        description: "lastname of user",
        required: false,
        type: String,
    })
    lastname?: string;

    @ApiModelProperty({
        description: "civility of user",
        required: false,
        type: String,
    })
    civility?: string;

    @ApiModelProperty({
        description: "address of user",
        required: false,
        type: String,
    })
    address?: string;

    @ApiModelProperty({
        description: "birthDate of user",
        required: false,
        type: Date,
    })
    birthDate?: Date;
}
