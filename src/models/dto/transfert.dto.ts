import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';
import { AccountDto } from './account.dto';

export class TransfertDto {
    @ApiModelPropertyOptional()
    id?: string;

    @ApiModelProperty()
    accountId: string;

    @ApiModelProperty({ type: () => AccountDto })
    account: AccountDto;

    @ApiModelProperty()
    targetAccountNb: string;

    @ApiModelProperty()
    date: Date;

    @ApiModelProperty()
    amount: number;
}
