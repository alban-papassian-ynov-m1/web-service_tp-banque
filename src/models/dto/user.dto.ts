import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';
import { AccountDto } from './account.dto';

export class UserDto {
    @ApiModelPropertyOptional()
    id?: string;

    @ApiModelProperty()
    username: string;

    @ApiModelProperty()
    password: string;

    @ApiModelProperty()
    firstname: string;

    @ApiModelProperty()
    lastname: string;

    @ApiModelProperty()
    civility: string;

    @ApiModelProperty()
    birthDate: Date;

    @ApiModelPropertyOptional({ type: () => AccountDto, isArray: true })
    accounts?: AccountDto[];
}
