import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';
import { AccountDto } from './account.dto';

export class BankDto {
    @ApiModelPropertyOptional()
    id?: string;

    @ApiModelProperty()
    name: string;

    @ApiModelProperty()
    number: string;

    @ApiModelPropertyOptional({ type: () => AccountDto, isArray: true })
    accounts?: AccountDto[];
}
