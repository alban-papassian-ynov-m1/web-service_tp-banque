import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';
import { UserDto } from './user.dto';
import { BankDto } from './bank.dto';
import { DepositOrWithdrawalDto } from './deposit-or-withdrawal.dto';
import { TransfertDto } from './transfert.dto';

export class AccountDto {
    @ApiModelPropertyOptional()
    id?: string;

    @ApiModelProperty()
    number: string;

    @ApiModelProperty()
    pinCode: string;

    @ApiModelProperty()
    balance: number;

    @ApiModelProperty()
    limit: number;

    @ApiModelProperty()
    userId: string;

    @ApiModelProperty({ type: () => UserDto })
    user: UserDto;

    @ApiModelProperty()
    bankId: string;

    @ApiModelProperty({ type: () => BankDto })
    bank: BankDto;

    @ApiModelPropertyOptional({ type: () => TransfertDto, isArray: true })
    transferts?: TransfertDto[];

    @ApiModelProperty({ type: () => DepositOrWithdrawalDto, isArray: true })
    depositOrWithdrawals?: DepositOrWithdrawalDto[];
}
