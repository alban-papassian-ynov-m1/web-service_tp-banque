import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';
import { AccountDto } from './account.dto';

export class DepositOrWithdrawalDto {
    @ApiModelPropertyOptional()
    id?: string;

    @ApiModelProperty()
    accountId: string;

    @ApiModelProperty({ type: () => AccountDto })
    account: AccountDto;

    @ApiModelProperty()
    date: Date;

    @ApiModelProperty()
    amount: number;
}
