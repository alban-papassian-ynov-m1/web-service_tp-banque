import { ApiModelProperty } from '@nestjs/swagger';
import { GenericResponse } from './_generic.response';
import { DepositOrWithdrawalDto } from '../dto/deposit-or-withdrawal.dto';

export class GetDepositOrWithdrawalResponse extends GenericResponse {

    @ApiModelProperty()
    depositOrWithdrawal: DepositOrWithdrawalDto;
    constructor() {
        super();
        this.depositOrWithdrawal = null;
    }
}

export class GetDepositOrWithdrawalsResponse extends GenericResponse {

    @ApiModelProperty({ type: [DepositOrWithdrawalDto] })
    depositOrWithdrawals: DepositOrWithdrawalDto[];
    @ApiModelProperty()
    totalDepositOrWithdrawalsCount: number;
    constructor() {
        super();
        this.depositOrWithdrawals = [];
        this.totalDepositOrWithdrawalsCount = 0;
    }
}
