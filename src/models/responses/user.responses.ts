import { ApiModelProperty } from '@nestjs/swagger';
import { UserDto } from '../dto/user.dto';
import { GenericResponse } from './_generic.response';

export class GetUserResponse extends GenericResponse {

    @ApiModelProperty()
    user: UserDto;
    constructor() {
        super();
        this.user = null;
    }
}

export class GetUsersResponse extends GenericResponse {

    @ApiModelProperty({ type: [UserDto] })
    users: UserDto[];
    @ApiModelProperty()
    totalUsersCount: number;
    constructor() {
        super();
        this.users = [];
        this.totalUsersCount = 0;
    }
}
