import { ApiModelProperty } from '@nestjs/swagger';
import { GenericResponse } from './_generic.response';
import { TransfertDto } from '../dto/transfert.dto';

export class GetTransferResponse extends GenericResponse {

    @ApiModelProperty()
    transfer: TransfertDto;
    constructor() {
        super();
        this.transfer = null;
    }
}

export class GetTransfersResponse extends GenericResponse {

    @ApiModelProperty({ type: [TransfertDto] })
    transfers: TransfertDto[];
    @ApiModelProperty()
    totalTransfersCount: number;
    constructor() {
        super();
        this.transfers = [];
        this.totalTransfersCount = 0;
    }
}
