# WEB SERVICE : TP BANQUE

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

## Introduction
NodeJs, NestJS, TypeOrm, Swagger

## Organisation de l'API

Le format des données est effectué au format JSON.

### Controller
Les controllers représentent les points d'entrés de l'API. 
Chaque controller possède 4 méthode et donc 4 points d'entrés : `GetOne`, `GetAll`, `CreateOrUpdate`, `Delete`.
Chacune des méthodes permet de faire appel aux méthodes correspondandant du service associé.

    Un seul point d'entré est définie pour la création et la mise à jour d'un élément. En effet, le traitement étant assez similaire pour ces 2 cas, le choix à donc était porté vers une méthode unique.

A savoir :
- GetOne: Récupère une entité en fonction de son id
- GetAll : Récupère tous les entités
- CreateOrUpdate : Créer ou met à jour une entité
- Delete : Supprime une entité en fonction de son id

### Services
Les services permettent d'écrire le traitement métier appliqué à chacunes des méthodes.
C'est dans ces services que la communication avec la base de donnée est effectué.

### Responses
Chacune des méthodes du controller renvois une `response`, en fonction du besoins.
Toute les `responses` sont des `extends` d'un type generic qui défini les élements classiques, qui doivent être présent dans chacune des `reponses`. (success, message, error, statusCode)

Pour une entité, 2 types de `reponse` est définie :
- Celle qui renvois un objet seul
- Celle qui renvois un tableau d'objet, ainsi que la taille de ce tableau

### Dto
Pour chaque entité est définie un DTO associé.
Cela permet d'avoir accès à des interfaces qui représente chacun des objets du projet.

### Entity
Toute les entités sont déclaré dans le dossier entities. Pour chacune de ces entités, on retrouve 2 méthodes : `fromDto` et `toDto`.
Ces méthodes permettent de `mapper` l'entité (coté bdd) vers un DTO (côté objet), et vice versa.

## Contrat d'interface

L'objectif de ce document est de recenser l'ensemble des routes disponibles dans l'API .
On y retrouve l'URL via laquelle on peux utiliser l'API, la méthode associé, les paramètres à fournir lors de l'appel ainsi que le type de retour.

| route | method | params | response | comment |
|--|--|--|--|--|
| login | post | { username: string, password: string } | GenericResponse | Permet de générer un token pour un utilisateur si les paramètres fournis correspondent à un compte existant |
| register | post | { civility: string, firstname: string, lastname: string, birthDate: date, address: string } | GenericResponse | Permet de créer un compte bancaire ainsi qu'un compte utilisateur si les paramètres sont valides |
|  |  |  |  |  |
| transfer | post | { sourceAccountNb: int, targetAccountNb: int, amount: float } | TransferResponse | Permet d'effectuer un virement entre 2 comptes d'un montant définie |
| transfer | get |  | TransfersResponse | Permet de récupérer tous les virements effectués |
| transfer/:id | get |  | TransferResponse | Permet de récupérer un virement en fonction de son id |
|  |  |  |  |  |
| deposit-or-withdrawal | post | { accountNumber: int, date: date, amount: float } | DepositOrWithdrawalResponse | Permet d'effectuer un dépot ou un retrait (montant négatif ou positif) sur un compte |
| deposit-or-withdrawal | get |  | DepositOrWithdrawalsResponse | Permet de récupérer tous les dépots ou retraits effectués |
| deposit-or-withdrawal/:id | get |  | DepositOrWithdrawalResponse | Permet de récupérer un dépot ou un retrait effectué en fonction de son id |

## Authentification

Afin de gérer la sécurité de l'API, la librairie Passport est utilisé pour authentifier les utilisateurs. Il s'agit de la librairie la plus populaire pour NodeJS et s'intègre parfaitement avec Nest. Elle permet de générer un JSON Web Token (JWT) qui sera fournis à l'application client. Ce token devras alors être stocké dans le localStorage de navigateur par l'application client, et devras être fournis dans le header des requetes lors des appels à l'API.

### AuthController

Il s'agit du point d'entré de l'API pour les fonctionnalités d'authentification. Il possède 2 méthodes : `login` & `register`.<br /> 
Ces méthodes permettent de faire appelle aux méthodes du service associé.
En plus de ce service, un service `AuthToolsService` permet d'effectuer les actions lié au JWT :
- créer un token
- décoder un token
- récupérer le payload
- récupérer le JWT depuis le header ou la request

### JwtPayload

Il s'agit d'une interface dans laquel on définie ce que l'on souhaite avoir dans le payload.

### Clé secrète

La clé secrète est situé dans le fichier `contants.ts` qui se trouve dans le dossier `environments`.